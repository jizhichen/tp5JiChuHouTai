/*
Navicat MySQL Data Transfer

Source Server         : 本机数据库
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : dzm

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-11-08 17:15:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dzm_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `dzm_admin_menu`;
CREATE TABLE `dzm_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `menu_name` varchar(100) DEFAULT '' COMMENT '菜单名称',
  `menu_url` varchar(255) DEFAULT '' COMMENT '菜单URL',
  `menu_tag` varchar(30) DEFAULT '' COMMENT '菜单标签',
  `menu_sort` int(11) DEFAULT '1' COMMENT '菜单排序',
  `menu_remark` varchar(255) DEFAULT '' COMMENT '菜单备注',
  `uuid` int(11) DEFAULT '0' COMMENT '修改人',
  `userid` int(11) DEFAULT '0' COMMENT '商家id',
  `cdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `udate` int(11) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1->有效,-1->删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_admin_menu
-- ----------------------------
INSERT INTO `dzm_admin_menu` VALUES ('1', '0', '系统管理', '', 'system', '1', '系统管理', '1', '1', '1510019830', '1510123075', '1');
INSERT INTO `dzm_admin_menu` VALUES ('2', '1', '角色管理', '/admin/role/index', 'role', '4', '角色管理', '2', '1', '1510021641', '1510054723', '1');
INSERT INTO `dzm_admin_menu` VALUES ('3', '1', '权限管理', '', 'permission', '1', '权限管理', '1', '1', '1510021691', '1510021691', '-1');
INSERT INTO `dzm_admin_menu` VALUES ('4', '1', '菜单管理', '/admin/menu/index', 'menu', '2', '菜单管理', '1', '1', '1510021712', '1510105994', '1');
INSERT INTO `dzm_admin_menu` VALUES ('5', '1', '用户管理', '/admin/user/index', 'user', '3', '用户管理', '2', '1', '1510024651', '1510054717', '1');
INSERT INTO `dzm_admin_menu` VALUES ('6', '5', '用户列表', '', 'userlist', '1', '用户列表', '1', '1', '1510024670', '1510024670', '-1');
INSERT INTO `dzm_admin_menu` VALUES ('7', '0', '订单管理', '', 'order', '1', '订单管理', '1', '1', '1510024685', '1510024685', '-1');
INSERT INTO `dzm_admin_menu` VALUES ('8', '7', '订单列表', '', 'orderlist', '1', '订单列表', '1', '1', '1510024703', '1510024703', '1');
INSERT INTO `dzm_admin_menu` VALUES ('9', '0', '报表管理', '', 'report', '1', '报表管理', '1', '1', '1510024738', '1510024738', '-1');
INSERT INTO `dzm_admin_menu` VALUES ('10', '0', '运营管理', '', 'operate', '4', '运营管理', '1', '1', '1510111179', '1510111179', '1');
INSERT INTO `dzm_admin_menu` VALUES ('11', '10', '区域管理', '/admin/areas/index', 'areas', '6', '区域管理', '1', '1', '1510111260', '1510111260', '1');
INSERT INTO `dzm_admin_menu` VALUES ('12', '10', '标签管理', '/admin/labels/index', 'labels', '7', '标签管理', '1', '1', '1510111321', '1510111321', '-1');

-- ----------------------------
-- Table structure for dzm_admin_menu_permissions
-- ----------------------------
DROP TABLE IF EXISTS `dzm_admin_menu_permissions`;
CREATE TABLE `dzm_admin_menu_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT '0' COMMENT '菜单id',
  `permissions_name` varchar(80) DEFAULT '' COMMENT '功能名称',
  `permissions_tag` varchar(80) DEFAULT '' COMMENT '功能代码',
  `permissions_sort` int(11) DEFAULT '0' COMMENT '权限排序',
  `permissions_remark` varchar(255) DEFAULT '' COMMENT '描述',
  `cdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `udate` int(11) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态(1->可用,-1->删除)',
  `userid` int(11) DEFAULT '0' COMMENT '商家id',
  `uuid` int(11) DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_admin_menu_permissions
-- ----------------------------
INSERT INTO `dzm_admin_menu_permissions` VALUES ('1', '4', '新建', 'add', '0', '新建', '1510032732', '1510032749', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('2', '4', '编辑', 'edit', '0', '编辑', '1510032772', '1510032772', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('3', '4', '删除', 'del', '0', '删除', '1510032786', '1510033875', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('4', '2', '新建', 'add', '0', '新建', '1510054269', '1510054269', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('5', '2', '编辑', 'edit', '0', '编辑', '1510054283', '1510054283', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('6', '2', '删除', 'del', '0', '删除', '1510054297', '1510054297', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('7', '5', '新建', 'add', '0', '新建', '1510054333', '1510054333', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('8', '5', '编辑', 'edit', '0', '编辑', '1510054344', '1510054344', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('9', '5', ' 删除', 'del', '0', ' 删除', '1510054359', '1510054359', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('10', '4', '详情', 'details', '0', '详情', '1510055565', '1510123088', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('11', '5', '详情', 'details', '0', '详情', '1510055587', '1510055587', '1', '1', '1');
INSERT INTO `dzm_admin_menu_permissions` VALUES ('12', '2', '详情', 'details', '0', '详情', '1510055617', '1510055617', '1', '1', '1');

-- ----------------------------
-- Table structure for dzm_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `dzm_admin_role`;
CREATE TABLE `dzm_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '0' COMMENT '用户id',
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `role_name` varchar(100) DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1->可用,-1->删除)',
  `cdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `udate` int(11) DEFAULT '0' COMMENT '更新时间',
  `uuid` int(11) DEFAULT '0' COMMENT '修改人',
  `desc` varchar(255) DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_admin_role
-- ----------------------------
INSERT INTO `dzm_admin_role` VALUES ('4', '1', '7', '运营', '1', '1510041990', '1510123447', '1', '运营');
INSERT INTO `dzm_admin_role` VALUES ('5', '1', '7', '市场', '1', '1510042015', '1510056260', '1', '市场');
INSERT INTO `dzm_admin_role` VALUES ('6', '2', '7', '开发', '1', '1510056027', '1510056828', '1', '开发');
INSERT INTO `dzm_admin_role` VALUES ('7', '1', '0', '总经理', '1', '1510056235', '1510104439', '1', '总经理');

-- ----------------------------
-- Table structure for dzm_admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `dzm_admin_role_permissions`;
CREATE TABLE `dzm_admin_role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '0' COMMENT '用户id',
  `menuid` int(11) NOT NULL DEFAULT '0' COMMENT '模块',
  `roleid` int(11) NOT NULL DEFAULT '0' COMMENT '角色',
  `permissionsid` int(11) DEFAULT '0' COMMENT '权限id',
  `permissions_name` varchar(255) DEFAULT '' COMMENT '权限名称',
  `attribute` varchar(80) NOT NULL DEFAULT '' COMMENT '属性(view->读,edit->编辑,delete->删除)',
  `cdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `udate` int(11) DEFAULT '0' COMMENT '更新时间',
  `uuid` int(11) DEFAULT '0' COMMENT '修改人',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态(-1->删除,1->可用)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=876 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_admin_role_permissions
-- ----------------------------
INSERT INTO `dzm_admin_role_permissions` VALUES ('849', '1', '4', '4', '1', '新建', 'add', '1510041990', '1510123447', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('850', '1', '4', '5', '1', '新建', 'add', '1510042015', '1510042718', '1', '-1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('851', '1', '4', '5', '2', '编辑', 'edit', '1510042015', '1510056260', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('852', '1', '4', '5', '3', '删除', 'del', '1510042015', '1510042715', '1', '-1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('853', '0', '4', '4', '2', '编辑', 'edit', '1510053807', '1510123447', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('854', '0', '4', '4', '3', '删除', 'del', '1510053807', '1510123447', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('855', '0', '5', '4', '7', '新建', 'add', '1510054409', '1510055629', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('856', '0', '5', '4', '8', '编辑', 'edit', '1510054409', '1510055629', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('857', '0', '5', '4', '9', '删除', 'del', '1510054409', '1510055629', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('858', '0', '2', '4', '4', '新建', 'add', '1510054421', '1510055648', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('859', '0', '2', '4', '5', '编辑', 'edit', '1510054421', '1510055648', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('860', '0', '2', '4', '6', '删除', 'del', '1510054421', '1510055648', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('861', '0', '5', '4', '11', '详情', 'details', '1510055629', '1510055629', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('862', '0', '4', '4', '10', '详情', 'details', '1510055640', '1510123447', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('863', '0', '2', '4', '12', '详情', 'details', '1510055648', '1510055648', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('864', '0', '4', '6', '1', '新建', 'add', '1510056717', '1510056788', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('865', '0', '4', '6', '2', '编辑', 'edit', '1510056717', '1510056788', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('866', '0', '4', '6', '3', '删除', 'del', '1510056717', '1510056788', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('867', '0', '4', '6', '10', '详情', 'details', '1510056717', '1510056788', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('868', '0', '5', '6', '7', '新建', 'add', '1510056820', '1510056820', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('869', '0', '5', '6', '8', '编辑', 'edit', '1510056820', '1510056820', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('870', '0', '5', '6', '9', '删除', 'del', '1510056820', '1510056820', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('871', '0', '5', '6', '11', '详情', 'details', '1510056820', '1510056820', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('872', '0', '2', '6', '4', '新建', 'add', '1510056828', '1510056828', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('873', '0', '2', '6', '5', '编辑', 'edit', '1510056828', '1510056828', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('874', '0', '2', '6', '6', '删除', 'del', '1510056828', '1510056828', '1', '1');
INSERT INTO `dzm_admin_role_permissions` VALUES ('875', '0', '2', '6', '12', '详情', 'details', '1510056828', '1510056828', '1', '1');

-- ----------------------------
-- Table structure for dzm_admin_users
-- ----------------------------
DROP TABLE IF EXISTS `dzm_admin_users`;
CREATE TABLE `dzm_admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parentid` int(11) DEFAULT '0' COMMENT '用户父id',
  `is_admin` tinyint(1) DEFAULT '1' COMMENT '是否管理员（1->是,2->否）',
  `picid` int(11) DEFAULT '0' COMMENT '头像',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户名',
  `user_password` varchar(100) DEFAULT '' COMMENT '密码',
  `real_name` varchar(30) DEFAULT '' COMMENT '真实姓名',
  `sex` tinyint(1) DEFAULT '0' COMMENT '性别(1->男,2->女)',
  `weight` int(11) DEFAULT '1' COMMENT '权重',
  `date_birth` int(11) DEFAULT '0' COMMENT '出生日期',
  `date_entry` int(11) DEFAULT '0' COMMENT '入职日期',
  `phone` varchar(50) DEFAULT '' COMMENT '联系电话',
  `email` varchar(100) DEFAULT '' COMMENT '邮箱',
  `roleid` int(11) DEFAULT '0' COMMENT '角色',
  `rolename` varchar(80) DEFAULT '' COMMENT '角色名称',
  `openid` varchar(255) DEFAULT '' COMMENT '微信id',
  `cdate` int(11) DEFAULT '0' COMMENT '创建时间',
  `udate` int(11) DEFAULT '0' COMMENT '更新日期',
  `uuid` int(11) DEFAULT '0' COMMENT '修改人',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1->有效,-1->删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of dzm_admin_users
-- ----------------------------
INSERT INTO `dzm_admin_users` VALUES ('1', '0', '1', '0', 'admin', '96e79218965eb72c92a549dd5a330112', '多姿蜜', '1', '1', '0', '0', '', '', '0', '', '', '1509958442', '1509958442', '0', '1');
INSERT INTO `dzm_admin_users` VALUES ('2', '1', '2', '0', 'zhangyong', '96e79218965eb72c92a549dd5a330112', '张勇', '1', '1', '387820800', '0', '18621626982', 'zy19820417@163.com', '6', '   |--开发', '', '1510046495', '1510123443', '1', '1');

-- ----------------------------
-- Table structure for dzm_areas
-- ----------------------------
DROP TABLE IF EXISTS `dzm_areas`;
CREATE TABLE `dzm_areas` (
  `areaid` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(60) DEFAULT '',
  `area_pid` int(11) DEFAULT '0',
  `area_level` tinyint(1) DEFAULT '0',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '(1->可用.-1->删除）',
  `uuid` int(11) DEFAULT '0',
  `weight` int(11) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  PRIMARY KEY (`areaid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_areas
-- ----------------------------
INSERT INTO `dzm_areas` VALUES ('1', '上海', '0', '0', '1510129783', '1510130891', '-1', '1', '1', '1');
INSERT INTO `dzm_areas` VALUES ('2', '北京', '0', '0', '1510130257', '1510132064', '1', '1', '1', '1');
INSERT INTO `dzm_areas` VALUES ('3', '广州', '0', '0', '1510131048', '1510131048', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for dzm_file_data
-- ----------------------------
DROP TABLE IF EXISTS `dzm_file_data`;
CREATE TABLE `dzm_file_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_types` int(11) DEFAULT '0' COMMENT '(1->图片,2->音频,3->视频)',
  `file_from` int(11) DEFAULT '0' COMMENT '(1->网易,2->阿里云)',
  `file_url` varchar(255) DEFAULT '',
  `cdate` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_file_data
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_labels
-- ----------------------------
DROP TABLE IF EXISTS `dzm_labels`;
CREATE TABLE `dzm_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT '',
  `pid` int(11) DEFAULT '0',
  `level` tinyint(1) DEFAULT '1',
  `label_unit` tinyint(1) DEFAULT '0' COMMENT '(1->次,2->小时)',
  `top_price` int(11) DEFAULT '0',
  `lower_price` int(11) DEFAULT '0',
  `video_switch` tinyint(1) DEFAULT '0' COMMENT '(0->关,1->开)',
  `audio_switch` tinyint(1) DEFAULT '0' COMMENT '(0->关,1->开)',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1' COMMENT '(1->可用,-1->删除)',
  `is_default` int(1) DEFAULT '0' COMMENT '(0->否,1->是)',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_labels
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_order
-- ----------------------------
DROP TABLE IF EXISTS `dzm_order`;
CREATE TABLE `dzm_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `singleid` int(11) DEFAULT '0',
  `single_person` int(11) DEFAULT '0',
  `receiving_person` int(11) DEFAULT '0',
  `order_longtime` int(11) DEFAULT '0',
  `order_status` int(1) DEFAULT '0' COMMENT '(0->未开始,1->进行中,2->已完成)',
  `pay_type` int(1) DEFAULT '0' COMMENT '(1->支付宝,2->微信支付)',
  `pay_status` int(1) DEFAULT '0' COMMENT '(0->未支付,1->已支付)',
  `total` float(11,2) DEFAULT '0.00',
  `amount` float(11,2) DEFAULT '0.00',
  `start_time` int(11) DEFAULT '0',
  `end_time` int(11) DEFAULT '0',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `single_over_person` int(11) DEFAULT '0',
  `single_over_status` int(1) DEFAULT '0' COMMENT '(1->发单人中止,2->接单人中指,3->协商终止)',
  `uuid` int(11) DEFAULT '0',
  `status` int(1) DEFAULT '1' COMMENT '(1->可用,-1->删除)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_order
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_users
-- ----------------------------
DROP TABLE IF EXISTS `dzm_users`;
CREATE TABLE `dzm_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qqid` varchar(255) DEFAULT '',
  `wxid` varchar(255) DEFAULT '',
  `user_name` varchar(80) DEFAULT '',
  `user_passwd` varchar(80) DEFAULT '',
  `sex` tinyint(1) DEFAULT '0' COMMENT '（0-> 未知,1->男,2->女）',
  `nickname` varchar(80) DEFAULT '',
  `cityid` int(11) DEFAULT '0',
  `phone` varchar(80) DEFAULT '',
  `picid` int(11) DEFAULT '0',
  `onlie_status` tinyint(1) DEFAULT '0' COMMENT '（0->否,1->是）',
  `birth_age` int(2) DEFAULT '0',
  `Invite_code` int(11) DEFAULT '0',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` int(1) DEFAULT '1' COMMENT '(1->可用,-1-> 删除 )',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_1` (`Invite_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of dzm_users
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_users_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `dzm_users_dynamic`;
CREATE TABLE `dzm_users_dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '0',
  `content` varchar(255) DEFAULT '',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_users_dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_users_dynamic_lists
-- ----------------------------
DROP TABLE IF EXISTS `dzm_users_dynamic_lists`;
CREATE TABLE `dzm_users_dynamic_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dynamicid` int(11) DEFAULT '0',
  `fileid` int(11) DEFAULT '0',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT '(1->可用,-1->删除)',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_users_dynamic_lists
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_users_labels
-- ----------------------------
DROP TABLE IF EXISTS `dzm_users_labels`;
CREATE TABLE `dzm_users_labels` (
  `id` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) DEFAULT '0',
  `labelid` int(11) DEFAULT '0',
  `iscustom` tinyint(1) DEFAULT '0' COMMENT '(0->否,1->是)',
  `custom_label_name` varchar(80) DEFAULT '',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0' COMMENT '(1->可用,-1->删除)',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_users_labels
-- ----------------------------

-- ----------------------------
-- Table structure for dzm_users_single
-- ----------------------------
DROP TABLE IF EXISTS `dzm_users_single`;
CREATE TABLE `dzm_users_single` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '0',
  `single_content` varchar(255) DEFAULT '',
  `cityid` int(11) DEFAULT '0',
  `long_time` float(11,1) DEFAULT '0.0',
  `price` float(11,2) DEFAULT '0.00',
  `labelid` int(11) DEFAULT '0',
  `single_desc` varchar(255) DEFAULT '',
  `cdate` int(11) DEFAULT '0',
  `udate` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '(1->可用,-1->删除)',
  `single_type` tinyint(1) DEFAULT '0' COMMENT '(1->次,2->小时）',
  `uuid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dzm_users_single
-- ----------------------------
