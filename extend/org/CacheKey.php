<?php
/*
 * @author zhangyong
 * 临时缓存key的统一接口
 */
namespace org;
class CacheKey
{
	private $prefix;
    // 后台区域 admin_areas_key
    public function get_admin_areas_key($id)
    {
    	$this->prefix=config('cache.prefix').'_';
        return $this->prefix.__FUNCTION__ . $id;
    }
}
