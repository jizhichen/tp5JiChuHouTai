<?php
/*
 * 图片存储表
 */
namespace org;
use think\Controller;
use think\Db;
use think\template\driver\File;
use org\Image;
use org\image\driver\Gd;
use org\Upload; //上传类
use org\CacheKey; //缓存类
use app\index\model\PicDataModel;
class PicData
{
	public $md5_file_name; //文件md5值
	public function upload_file($souce_file,$thum=false,$width="100",$height="100",$watermark)
	{
		if($thum)
		{
			$thumname=$this->saveName($souce_file);
			$orgname=$thumname;
			$orgpath=config('DATA_UPLOAD').$this->explode_md5($souce_file,false).'/';
			$thumpath=config('DATA_UPLOAD').$this->explode_md5($souce_file,true).'/';
			$thumdatapath=$this->explode_md5($souce_file,true).'/';
			$this->make_dir($thumpath);
		}
		$config = array(
            'mimes' => [], //允许上传的文件MiMe类型
            'maxSize' =>2014000, //上传的文件大小限制 (0-不做限制)
            //'exts' => $ftype, //允许上传的文件后缀
            'autoSub' => true, //自动子目录保存文件
            'subName' => $this->explode_md5($souce_file,false),
            'rootPath' => config('DATA_UPLOAD'), //保存根路径
			'saveName' => $this->saveName($souce_file),
            'savePath' => '', //保存路径
        );
		$upload = new \org\Upload($config,'LOCAL');
		$images = $upload->upload();
		$data['org']= $images['Filedata']['savepath'].$images['Filedata']['savename'];
		//缩略图
		if($thum)
		{
			   $image = new \Think\Image();
			   $image->open(config('DATA_UPLOAD').$data['org']);
			   $thumext='.'.substr(strrchr($data['org'], '.'), 1);
			   $image->thumb($width, $height)->save($thumpath.$thumname.$thumext);
			   $data['thum']=$thumdatapath.$thumname.$thumext;
		}
		//水印
		if($watermark)
		{
			   $image = new \Think\Image();
			   $thumext='.'.substr(strrchr($data['org'], '.'), 1);
			   //文字
			  //  $image->open(config('DATA_UPLOAD').$data['org'])->text('张洹',config('DATA_UPLOAD').'font/msyhbd.ttf',80,'#000000',\Think\Image::IMAGE_WATER_SOUTHEAST)->save($thumpath.$thumname.$thumext);
			  //  $image->open(config('DATA_UPLOAD').$data['org'])->text('张洹',config('DATA_UPLOAD').'font/msyhbd.ttf',80,'#000000',\Think\Image::IMAGE_WATER_SOUTHEAST)->save($orgpath.$thumname.$thumext);
			   //图片
			   $image->open(config('DATA_UPLOAD').$data['org'])->water(__static__.'/admin/images/logo.png',\Think\Image::THINKIMAGE_WATER_SOUTHEAST,50)->save($orgpath.$thumname.$thumext);
		}
		$data['cdate']=time();
		$m = new  PicDataModel();
		$id=$m->insert_data($data);
		if($id)
		{
			$info['id']=$id;
			$info['org']= config("IMG_DOMAIN").$data['org'];
		}
		return $info;
	}

	/**
	 * 拆分md5字符串
	 */
	public function explode_md5($souce_file,$thum)
		{
			if(!$souce_file)return false;
			$this->md5_file_name=md5_file($souce_file);
			$arr[1]=substr($this->md5_file_name, 0, 1);
			$arr[2]=substr($this->md5_file_name, 1, 2);
			$arr[3]=substr($this->md5_file_name, 3, 3);
			$arr[4]=substr($this->md5_file_name, 6, 4);
			$arr[5]=substr($this->md5_file_name, 10);
			if($thum)
			{
				$nfs_url= '/thum/'.$arr[1].'/'.$arr[2].'/'.$arr[3].'/'.$arr[4];
			}else{
				$nfs_url= '/org/'.$arr[1].'/'.$arr[2].'/'.$arr[3].'/'.$arr[4];
			}
			return $nfs_url;
		}

		/**
	 	* 图片名称
	 	*/
	    public function saveName($souce_file)
		{
			if(!$souce_file)return false;
			$this->md5_file_name=md5_file($souce_file);
			$saveName=substr($this->md5_file_name, 10);
			return $saveName.date('is', time()).mt_rand(0,9);
		}

		/**
	 	* 获取图片单条记录
	 	* $id 图片表pic_data主键
		* 返回值  数组   exa:  array("id"=>"1","org"=>"http://www.xxx.com/xxx.jpg");
	 	*/
		public function get_row_byid($id)
		{
			return $data;
		}

	    /**
		 * 创建目录
		 */
		public function make_dir($dir='')
		{
			return is_dir($dir) or ($this->make_dir(dirname($dir)) and mkdir($dir, 0777));
		}
    public function delFileResource($img_path){
        $filepath = config('DATA_UPLOAD').$img_path;
        if(file_exists($filepath)){
//            echo $filepath;exit;
           return @unlink($filepath);
        }
        return false;
    }
}
?>
