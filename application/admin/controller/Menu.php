<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use org\Condition;
use org\Page;
class Menu extends Common
{
    public $check_access=true; //页面登陆权限
    public function index()
    {
        $access=getuserpermissions('Menu');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $edit=getuserpermissions('Menu','edit');
        $del=getuserpermissions('Menu','del');
        $add=getuserpermissions('Menu','add');
        $userinfor=session('userinfor');
        $keywords = input('param.keywords');
        $pagenumber = input('param.pagenumber','15');
        $where="status=1 ";
        if($keywords){
            $where.=" and (menu_name like '%".$keywords."%' or menu_remark like '%".$keywords."%') ";
        }
        $m = db_func("admin_menu", "dzm_");
        $count = $m->where($where)->count();
        $Page = new  \org\Page($count, $pagenumber);
        $Page->parameter["keywords"] = $keywords;
        $show = $Page->show();
        $orderby['menu_sort']='asc';
        $list = Db::table('dzm_admin_menu')->where($where)->order($orderby)->limit($Page->firstRow.','.$Page->listRows)->select();
        if($list) {
            $list = getTreeLevel($list, $html = '--', $pid = $list[0]['pid'], $level = 0);
        }
        $this->assign('edit',$edit);
        $this->assign('del',$del);
        $this->assign('add',$add);
        $this->assign('count',$count);
        $this->assign('page',$show);
        $this->assign('pagenumber',$pagenumber);
        $this->assign('keywords',$keywords);
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function edit()
    {
        $access=getuserpermissions('Menu','edit');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $list=array();
        $menu=array();
        $tree=array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $id = input('param.id');
        if($id)
        {
            $m = db_func("admin_menu", "dzm_");
            $list=$m->where("id=$id and status=1")->find();
        }
        $where="status=1";
        $menu = Db::table('dzm_admin_menu')->where($where)->select();
        if($menu){
            $tree=getTreeLevel($menu, $html = '--', $pid = 0, $level = 0);
        }
        $this->assign('list',$list);
        $this->assign('tree',$tree);
        return $this->fetch();
    }

    public function editAjax(){
        $data=array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $post = input("post.");
        extract($post);
        $data['menu_name'] = $menu_name;
        $data['pid'] = $menuid;
        $data['menu_tag'] = $menu_tag;
        $data['menu_url'] = $menu_url;
        $data['menu_sort'] = $menu_sort;
        $data['menu_remark'] = $menu_remark;
        $data['uuid']=$userid;
        $data['udate']=time();
        if(!empty($menu_name)&&empty($id))
        {
            $data['cdate']=time();
            $data['userid'] = $userid;
            $m = db_func("admin_menu", "dzm_");
            $id=$m->where("menu_name='".$menu_name."' and status=1")->value('id');
            if($id){
                wrong_return('标签已存在',2);
            }else{
                $add = getuserpermissions('Menu', 'add');
                if($add){
                    $res=Db::table('dzm_admin_menu')->insert($data);
                    if($res){
                        ok_return('保存成功',1);
                    }
                }
            }
        }elseif($id){
            $m = db_func("admin_menu", "dzm_");
            $r=$m->where("menu_name='".$menu_name."' and id!=$id and status=1")->value('id');
            if($r){
                wrong_return('标签已存在',2);
            }else{
                $edit= getuserpermissions('Menu', 'edit');
                if($edit){
                    $rst=Db::table('dzm_admin_menu')
                        ->where('id','eq',$id)
                        ->update($data);
                    if($rst){
                        ok_return('保存成功',3);
                    }
                }
            }
        }
    }

    public function details()
    {
        $access=getuserpermissions('Menu','details');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $list=array();
        $fun=array();
        $id = input('param.id');
        if($id)
        {
            $m = db_func("admin_menu", "dzm_");
            $list=$m->where("id=$id and status=1")->find();
            $orderby['udate']='asc';
            $where=" status=1 and id=$id  ";
            $fun = Db::table('dzm_admin_menu_permissions')->where($where)->order($orderby)->select();
        }
        $this->assign('list',$list);
        $this->assign('fun',$fun);
        return $this->fetch();
    }

    public function delAjax(){
        $del= getuserpermissions('Menu', 'del');
        $id = input('param.id');
        if(!empty($id)&&$del)
        {
            $data['status']=-1;
            $rst=Db::table('dzm_admin_menu')
                ->where('id','eq',$id)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
        $this->ajaxReturn($rst);
    }

    public function delMoreAjax(){
        $del= getuserpermissions('Menu', 'del');
        $ids = input('param.ids');
        if($ids&&$del)
        {
            $ids=substr($ids, 0,-1);
            $data['status']=-1;
            $rst=Db::table('dzm_admin_menu')
                ->where('id','in',$ids)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
    }


    public function fun(){
        $access=getuserpermissions('Menu');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $menuid = input('param.menuid',0);
        $keywords = input('param.keywords','');
        $pagenumber = input('param.pagenumber',15);
        //权限
        $edit=getuserpermissions('Menu','edit');
        $del=getuserpermissions('Menu','del');
        $add=getuserpermissions('Menu','add');
        $userinfor=session('userinfor');
        $where=" status=1 and  menuid=$menuid ";
        if($keywords){
            $where.=" and (permissions_name like '%".$keywords."%' or permissions_remark like '%".$keywords."%') ";
        }
        $mp = db_func("admin_menu_permissions", "dzm_");
        $count = $mp->where($where)->count();
        $Page = new  \org\Page($count, $pagenumber);
        $Page->parameter["keywords"] = $keywords;
        $Page->parameter["menuid"] = $menuid;
        $show       = $Page->show();
        $orderby['udate']='desc';
        if($menuid){
            $list = Db::table('dzm_admin_menu_permissions')->where($where)->order($orderby)->limit($Page->firstRow.','.$Page->listRows)->select();
            $m = db_func("admin_menu", "dzm_");
            $menu=$m->where("id=$menuid and status=1")->find();
        }
        $this->assign('list',$list);
        $this->assign('count',$count);
        $this->assign('page',$show);
        $this->assign('pagenumber',$pagenumber);
        $this->assign('keywords',$keywords);
        $this->assign('edit',$edit);
        $this->assign('del',$del);
        $this->assign('add',$add);
        $this->assign('menuid',$menuid);
        $this->assign('menu',$menu);
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function editfun(){
        $access=getuserpermissions('Menu','edit');
        if(!$access)
        {
            $this->error('您没有权限访问','');
        }
        $fun = array();
        $menuid = input('param.menuid',0);
        $funid = input('param.funid',0);
        $where=" status=1 ";
        $mp = db_func("admin_menu_permissions", "dzm_");
        if($menuid&&$funid){
            $fun=$mp->where("menuid=$menuid and id=$funid and status=1")->find();
        }
        $list = Db::table('dzm_admin_menu')->where($where)->select();
        $list=getTreeLevel($list, $html = '--', $pid = 0, $level = 0);
        $this->assign('menuid',$menuid);
        $this->assign('funid',$funid);
        $this->assign('list',$list);
        $this->assign('fun',$fun);
        return $this->fetch();
    }

    public function editFunAjax(){
        $data=array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $post = input("post.");
        extract($post);
        $data['menuid'] = $menuid;
        $data['permissions_name'] = $permissions_name;
        $data['permissions_tag'] = $permissions_tag;
        $data['permissions_remark'] = $permissions_remark;
        $data['uuid']=$userid;
        $data['udate']=time();
        if(!empty($permissions_name)&&empty($funid))
        {
            $data['cdate']=time();
            $data['userid'] = $userid;
            $mp = db_func("admin_menu_permissions", "dzm_");
            $id=$mp->where("menuid = $menuid and permissions_name='".$permissions_name."' and status=1")->value('id');
            if($id){
                wrong_return('功能已存在',2);
            }else{
                $add = getuserpermissions('Menu', 'add');
                if($add){
                    $res=Db::table('dzm_admin_menu_permissions')->insert($data);
                    if($res){
                        ok_return('保存成功',1);
                    }
                }
            }
        }elseif($funid){
            $mp = db_func("admin_menu_permissions", "dzm_");
            $r=$mp->where("menuid = $menuid and permissions_name='".$permissions_name."' and id!=".$funid." and status=1")->value('id');
            if($r){
                wrong_return('功能已存在',2);
            }else{
                $edit= getuserpermissions('Menu', 'edit');
                if($edit){
                    $rst=Db::table('dzm_admin_menu_permissions')
                        ->where('id','eq',$funid)
                        ->update($data);
                    if($rst){
                        ok_return('保存成功',3);
                    }
                }
            }
        }
    }

    public function delFunAjax(){
        $del= getuserpermissions('Menu', 'del');
        $id = input('param.id');
        if(!empty($id)&&$del)
        {
            $data['status']=-1;
            $rst=Db::table('dzm_admin_menu_permissions')
                ->where('id','eq',$id)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
        $this->ajaxReturn($rst);
    }

    public function delFunMoreAjax(){
        $del= getuserpermissions('Menu', 'del');
        $ids = input('param.ids');
        if($ids&&$del)
        {
            $ids=substr($ids, 0,-1);
            $data['status']=-1;
            $rst=Db::table('dzm_admin_menu_permissions')
                ->where('id','in',$ids)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
    }
}
