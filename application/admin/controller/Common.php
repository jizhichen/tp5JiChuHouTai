<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace app\admin\controller;
use think\Controller;
use think\Db;
class Common extends Controller {
        public $_cjv = 0;//config 中的 CSS_JS_VS 控制 js css 的版本号
        public $_site_url = '';//网站域名 http://wxxx.xxx.com
        public $_module='';
        public $_controller = '';//当前控制器
        public $_action = '';//当前操作
        public $_timestamp = '';
        public $check_access=true; //页面登陆权限
        public $pagesize = 100;
        public $imagedomain = '';
        protected function _initialize(){
            $longtime=relogin();
            if($longtime<=0 && $this->check_access==true )
            {
                session('userinfor',null);
                $this->redirect("Login/index");
            }elseif(session('?userinfor')){
                $data=session('userinfor');
                $data['logintime']=time()+7200;
                session('userinfor',$data);
                $userinfor=session('userinfor');
                if($userinfor['is_admin']==1){
                  $m = db_func("admin_menu", "dzm_");
                  $orderby['menu_sort']='asc';
                  $menulist=$m->where('status=1')->order($orderby)->select();
                    $menulist= getTree($menulist,$menulist[0]['pid']);
                 //用户(下级)
                }elseif($userinfor['is_admin']==2){
                    $menulist= array();
                   $roleid=$userinfor['roleid'];
                   $db_prefix='dzm_';
                   $table_menu=$db_prefix."admin_menu";
                   $table_role_permissions=$db_prefix."admin_role_permissions";
                   $query="SELECT * FROM $table_menu as m
                           WHERE m.status=1
                           and m.id in(
                               select distinct(menuid) as menuid from $table_role_permissions as p
                               where p.status=1 and p.roleid=$roleid
                           )
                           ORDER BY m.menu_sort ASC";
                    $m = db_func("admin_menu", "dzm_");
                    $data= $m->query($query);
                    $m = db_func("admin_menu", "dzm_");
                    $orderby['menu_sort']='asc';
                    $menus=$m->where('status=1')->order($orderby)->select();
                    $list = array();
                    foreach ($data as $key => $value) {
                        $list[$key]=getParents($menus,$value['id']);
                    }
                    // print_r($list);
                    if($list) {
                        $res = array();
                        foreach ($list as $item) {
                            $res = array_merge($res, $item);
                        }
                        //合并需要合并的俩个数组
                        $key = 'id';//去重条件
                        $tmp_arr = array();//声明数组
                        foreach ($res as $k => $v) {
                            if (in_array($v[$key], $tmp_arr)) //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true
                            {
                                unset($res[$k]);
                                //删除掉数组（$arr）里相同ID的数组
                            } else {
                                $tmp_arr[] = $v[$key];
                                //记录已有的id
                            }
                        }
                        $menulist= getTree($res,$res[0]['pid']);
                    }
                }
                $this->imageupload=config('IMG_UPLOAD').'/upload/upload/index';
                $this->assign('imgupload',$this->imageupload);
                $this->assign('menulist', $menulist);
                $this->assign('userinfor',$userinfor);
            }
        }
}
?>
