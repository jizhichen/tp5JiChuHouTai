<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use org\Condition;
use org\Page;
use app\admin\model\AreasModel;
class Areas extends Common
{

    /**
     * 角色管理
     *
     */
    public function index()
    {
        $access = getuserpermissions('Areas');
        if (!$access) {
            die('您没有权限访问');
        }
        $edit = getuserpermissions('Areas', 'edit');
        $del = getuserpermissions('Areas', 'del');
        $add = getuserpermissions('Areas', 'add');
        $keywords = input('param.keywords');
        $pagenumber = input('param.pagenumber', '15');
        $userinfor = session('userinfor');
        $where = "status=1 ";
        if ($keywords) {
            $where .= " and (area_name like '%" . $keywords . "%') ";
        }
        $u = db_func("areas", "dzm_");
        $count = $u->where($where)->count();
        $Page = new  \org\Page($count, $pagenumber);
        $show = $Page->show();
        $orderby['udate'] = 'asc';
        $list = Db::table('dzm_areas')->where($where)->order($orderby)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        if($list) {
            foreach ($list as $key => &$value) {
              $value['id']=$value['areaid'];
              $value['pid']=$value['area_pid'];
            }
            $list = getTreeLevel($list, $html = '--', $pid = $list[0]['pid'], $level = 0);
        }
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $show);
        $this->assign('pagenumber', $pagenumber);
        $this->assign('keywords', $keywords);
        $this->assign('edit', $edit);
        $this->assign('del', $del);
        $this->assign('add', $add);
        return $this->fetch();
    }

    /**
     * 编辑角色信息
     *
     */
    public function edit()
    {
        $access = getuserpermissions('Areas', 'edit');
        if (!$access) {
            die('您没有权限访问');
        }
        $list = array();
        $id = input('param.id');
        if ($id) {
            //$m = db_func("areas", "dzm_");
            //$list = $m->where("areaid='" . $id . "' and status=1")->find();
            $am = new  AreasModel();
            $list = $am->get_row_byid($id);
        }
        $orderby['weight'] = 'asc';
        $areas = Db::table('dzm_areas')->where("status=1")->order($orderby)->select();
        if($areas){
            foreach ($areas as $key => &$value) {
              $value['id']=$value['areaid'];
              $value['pid']=$value['area_pid'];
            }
            $areas = getTreeLevel($areas, $html = '--', $pid = 0, $level = 0);
        }
        $this->assign('list', $list);
        $this->assign('areas', $areas);
        return $this->fetch();
    }


    public function editAjax(){
        $data=array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $post = input("post.");
        extract($post);
        $data['area_name'] = $areaname;
        $data['area_pid'] = $pid;
        $data['uuid']=$userid;
        $data['udate']=time();
        if(!empty($areaname)&&empty($id))
        {
            $data['cdate']=time();
            $data['userid'] = $userid;
            $m = db_func("areas", "dzm_");
            $id=$m->where("area_name='".$areaname."' and status=1")->value('areaid');
            if($id){
                wrong_return('区域已存在',2);
            }else{
                $add = getuserpermissions('Areas', 'add');
                if($add){
                    $am= new  AreasModel();
                    //$res=Db::table('dzm_areas')->insert($data);
                    $res=$am->insert_data($data);
                    if($res){
                        ok_return('保存成功',1);
                    }
                }
            }
        }elseif($id){
            $m = db_func("areas", "dzm_");
            $r=$m->where("area_name='".$areaname."' and areaid!=$id and status=1")->value('areaid');
            if($r){
                wrong_return('区域已存在',2);
            }else{
                $edit= getuserpermissions('Areas', 'edit');
                if($edit){
                    $am= new  AreasModel();
                    $rst=$am->update_row($data,$id);
                    // $rst=Db::table('dzm_areas')
                    //     ->where('areaid','eq',$id)
                    //     ->update($data);
                    if($rst){
                        ok_return('保存成功',3);
                    }
                }
            }
        }
    }

    public function delAjax(){
        $del= getuserpermissions('Areas', 'del');
        $id = input('param.id');
        if(!empty($id)&&$del)
        {
            $data['status']=-1;
            $am= new  AreasModel();
            // $rst=Db::table('dzm_areas')
            //     ->where('areaid','eq',$id)
            //     ->update($data);
            $rst = $am->delete_data($id);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
        $this->ajaxReturn($rst);
    }

    public function delMoreAjax(){
        $del= getuserpermissions('Areas', 'del');
        $ids = input('param.ids');
        if($ids&&$del)
        {
            $ids=substr($ids, 0,-1);
            $data['status']=-1;
            $ids = explode(',', $ids);
            $am= new  AreasModel();
            foreach ($ids as $key => $id) {
                 $rst = $am->delete_data($id);
            }
            // $rst=Db::table('dzm_areas')
            //     ->where('areaid','in',$ids)
            //     ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
    }
}
