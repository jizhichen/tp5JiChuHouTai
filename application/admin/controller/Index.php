<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
class Index extends Common
{
    public $check_access=true; //页面登陆权限
    public function index()
    {
        $userinfor=session('userinfor');
        $userid=$userinfor['id'];
        return $this->fetch();
    }
}
