<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use org\Verify;
class Login extends Controller
{
    public function index()
    {
        return $this->fetch();
    }


    /**
     * 验证码生成
     */
    public function verify_c()//验证码
    {
        ob_clean();//清缓存
        $config = [
            'seKey' => '', // 验证码加密密钥
            'codeSet' => '23456789', // 验证码字符集合
            'expire' => 1800, // 验证码过期时间（s）
            'useZh' => false, // 使用中文验证码
            'useImgBg' => false, // 使用背景图片
            'fontSize' => 30, // 验证码字体大小(px)
            'useCurve' => false, // 是否画混淆曲线
            'useNoise' => false, // 是否添加杂点
            'imageH' => 0, // 验证码图片高度
            'imageW' => 0, // 验证码图片宽度
            'length' => 4, // 验证码位数
            'fontttf' => '', // 验证码字体，不设置随机获取
        ];
        $verify = new Verify($config);
        $verify->entry();//输出保存session
        die();
    }


    /**
     * 验证码检查
     */
    function check_verify($code, $id = ""){
        $verify = new Verify();
        return $verify->check($code, $id);
    }


    public function login(){
        $post = input("post.");
        extract($post);
        if($username&&$password&&$verify){
            $remember = 0;
            if(!$this->check_verify($verify)){
                wrong_return('验证码错误',3);
            }else{
                $m = db_func("admin_users", "dzm_");
                $id=$m->where(['user_name' => $username])->where('status', 'neq', -1)->value("id");
                if(!$id)
                {
                    wrong_return('用户名或密码错误',2);
                }
                else
                {
                    $m = db_func("admin_users", "dzm_");
                    $data=$m->where(['user_name' => $username,'user_password'=>md5($password)])->where('status', 'neq', -1)->find();
                    // if($data && $data['validity_time']>time())
                    if($data)
                    {
                        //设置session
                        $data['logintime']=time()+config('SESSION_EXPIRE');
                        session('userinfor',$data);
                        //设置cookie
                        if($remember==1){
                            cookie('username',$username,10800);
                            cookie('password',encrypt($password,E),10800);
                        }else{
                            cookie('username',null);
                            cookie('password',null);
                        }
                        ok_return('登录成功',1);
                    }
                    // elseif($data && $data['validity_time']<time()){
                    // 	$rst=4;
                    // }
                    else{
                        wrong_return('用户名或密码错误',5);
                    }
                }
            }
        }
    }

    //退出用户中心
    public function logout(){
        //删除session
        session('userinfor',null);
        //删除cookie
        //cookie('username',null);
        //cookie('password',null);
        session_destroy();
        $this->redirect("Login/index");
    }
}
