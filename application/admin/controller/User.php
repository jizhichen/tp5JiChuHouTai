<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use org\Condition;
use org\Page;

class User extends Common
{

    /**
     * 用户管理
     *
     */
    public function index()
    {
        $access = getuserpermissions('User');
        if (!$access) {
            die('您没有权限访问');
        }
        $edit = getuserpermissions('User', 'edit');
        $del = getuserpermissions('User', 'del');
        $add = getuserpermissions('User', 'add');
        $keywords = input('param.keywords');
        $pagenumber = input('param.pagenumber', '15');
        $userinfor = session('userinfor');
        $where = "status=1 ";
        if ($keywords) {
            $where .= " and (user_name like '%" . $keywords . "%' or real_name like '%" . $keywords . "%' or phone like '%" . $keywords . "%') ";
        }
        $u = db_func("admin_users", "dzm_");
        $count = $u->where($where)->count();
        $Page = new  \org\Page($count, $pagenumber);
        $show = $Page->show();
        $orderby['weight'] = 'asc';
        $list = Db::table('dzm_admin_users')->where($where)->order($orderby)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $show);
        $this->assign('pagenumber', $pagenumber);
        $this->assign('keywords', $keywords);
        $this->assign('edit', $edit);
        $this->assign('del', $del);
        $this->assign('add', $add);
        return $this->fetch();
    }

    /**
     * 编辑用户信息
     *
     */
    public function add()
    {
        $access = getuserpermissions('User', 'add');
        if (!$access) {
            die('您没有权限访问');
        }
        $list = array();
        $id = input('param.id');
        if ($id) {
            $u = db_func("admin_users", "dzm_");
            $list = $u->where("id='" . $id . "' and status=1")->find();
        }
        $orderby['pid'] = 'asc';
        $role = Db::table('dzm_admin_role')->where("status=1")->order($orderby)->select();
        $role = getTreeLevel($role, $html = '--', $pid = 0, $level = 0);
        $this->assign('list', $list);
        $this->assign('role', $role);
        return $this->fetch();
    }

    /**
     * 用户详情
     *
     */
    public function details()
    {
        $access = getuserpermissions('User', 'details');
        if (!$access) {
            $this->error('您没有权限访问', '');
        }
        $list = array();
        $id = input('param.id');
        if ($id) {
            $u = db_func("admin_users", "dzm_");
            $list = $u->where("id='" . $id . "' and status=1")->find();
        }
        $this->assign('list', $list);
        return $this->fetch();
    }


    /**
     * 保存用户信息
     *
     */
    public function addAjax()
    {
        $data = array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $post = input("post.");
        extract($post);
        if ($password) {
            $password = md5($password);
            $data['user_password'] = $password;
        }
        $data['real_name'] = $real_name;
        $data['sex'] = $sex;
        $data['weight'] = $weight;
        if ($date_birth) {
            $data['date_birth'] = strtotime($date_birth);
        }
        $data['phone'] = $phone;
        $data['email'] = $email;
        if ($role) {
            $data['roleid'] = $role;
            $data['rolename'] = $rolename;
        }
        $data['is_admin'] = $is_admin;
        $data['uuid'] = $userid;
        $data['udate'] = time();
        if (!empty($user_name) && empty($id)) {
            if ($userinfor['parentid'] == 0) {
                $data['parentid'] = $userid;
            } else {
                $data['parentid'] = $userinfor['parentid'];
            }
            $data['user_name'] = $user_name;
            $data['cdate'] = time();;
            $u = db_func("admin_users", "dzm_");
            $id = $u->where("user_name='" . $user_name . "'")->value('id');
            if ($id) {
                wrong_return('用户已存在', 2);
            } else {
                $add = getuserpermissions('User', 'add');
                if ($add) {
                    $res = Db::table('dzm_admin_users')->insert($data);
                    if ($res) {
                        ok_return('保存成功', 1);
                    }
                }
            }
        } elseif ($id) {
            $edit = getuserpermissions('User', 'edit');
            if ($edit) {
                $res = Db::table('dzm_admin_users')
                    ->where('id', 'eq', $id)
                    ->update($data);
                if ($res) {
                    ok_return('保存成功', 3);
                }
            }
        }
    }

    /**
     * 删除用户
     *
     */
    public function delAjax()
    {
        $del = getuserpermissions('User', 'del');
        $id = input('param.id');
        if (!empty($id) && $del) {
            $data['status'] = -1;
            $res = Db::table('dzm_admin_users')
                ->where('id', 'eq', $id)
                ->update($data);
            if ($res) {
                ok_return('删除成功', 1);
            } else {
                wrong_return('删除失败', 2);
            }
        }
    }

    /**
     * 批量删除用户
     *
     */
    public function delMoreAjax()
    {
        $del = getuserpermissions('User', 'del');
        $ids = input('param.ids');
        if ($ids && $del) {
            $ids = substr($_POST['ids'], 0, -1);
            $data['status'] = -1;
            $rst = Db::table('dzm_admin_users')
                ->where('id', 'in', $ids)
                ->update($data);
            if ($rst) {
                ok_return('删除成功', 1);
            } else {
                wrong_return('删除失败', 2);
            }
        }
    }
}
