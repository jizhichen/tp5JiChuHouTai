<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use org\Condition;
use org\Page;
class Role extends Common
{
    public $check_access=true; //页面登陆权限
    public function index()
    {
        $access=getuserpermissions('Role');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $edit=getuserpermissions('Role','edit');
        $del=getuserpermissions('Role','del');
        $add=getuserpermissions('Role','add');
        $userinfor=session('userinfor');
        $keywords = input('param.keywords');
        $pagenumber = input('param.pagenumber','15');
        $where="status=1 ";
        if($keywords){
            $where.=" and (role_name like '%".$keywords."%') "; 
        }
        $m = db_func("admin_role", "dzm_");
        $count = $m->where($where)->count();
        $Page = new  \org\Page($count, $pagenumber);
        $Page->parameter["keywords"] = $keywords;
        $show = $Page->show();
        $orderby['udate']='asc';
        $list = Db::table('dzm_admin_role')->where($where)->order($orderby)->limit($Page->firstRow.','.$Page->listRows)->select();
        if($list) {
            $list = getTreeLevel($list, $html = '--', $pid = $list[0]['pid'], $level = 0);
        }
        $this->assign('edit',$edit);
        $this->assign('del',$del);
        $this->assign('add',$add);
        $this->assign('count',$count);
        $this->assign('page',$show);
        $this->assign('pagenumber',$pagenumber);
        $this->assign('keywords',$keywords);
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function edit()
    {
        $access=getuserpermissions('Role','edit');
        if(!$access)
        {
            die('您没有权限访问');
        }
        $list=array();
        $menu=array();
        $tree=array();
        $userinfor = session('userinfor');
        $userid = $userinfor['id'];
        $id = input('param.id');
        if($id)
        {
            $r = db_func("admin_role", "dzm_");
            $list=$r->where("id=$id and status=1")->find();
        }
        $where="status=1";
        $role = Db::table('dzm_admin_role')->where($where)->select();
        if($role){
            $tree=getTreeLevel($role, $html = '--', $pid = 0, $level = 0);
        }
        $orderby['menu_sort']='asc';
        $menu =Db::table('dzm_admin_menu')->where($where)->order($orderby)->select();
        if($menu){
            $menu=getTreeLevel($menu, $html = '--', $pid = 0, $level = 0);
        }
        $this->assign('list',$list);
        $this->assign('role',$role);
        $this->assign('menu',$menu);
        return $this->fetch();
    }

     public function editAjax(){
       $data=array();
       $userinfor = session('userinfor');
       $userid = $userinfor['id'];
       $post = input("post.");
       extract($post);
       $data['role_name'] = $rolename;
       $data['pid'] = $pid;
       $data['desc'] = $desc;
       $data['uuid']=$userid;
       $data['udate']=time();
       if($rolename&&!$id)
       {
            $data['cdate']=time();
            $data['userid'] = $userid;
            $r = db_func("admin_role", "dzm_");
            $id=$r->where("role_name='".$rolename."' and status=1")->value('id');
            if($id){
                wrong_return('角色已存在',2);
            }else{
                $add = getuserpermissions('Role', 'add');
                if($add){
                    $res=Db::table('dzm_admin_role')->insertGetId($data);
                    if($res){
                      if($funnum>0){
                          $funvals = explode('#', $funvals);
                          $funtexts = explode('#', $funtexts);
                          $funtags = explode('#', $funtags);
                          unset($data);
                          $data['status']='-1';
                          Db::table('dzm_admin_role_permissions')
                              ->where("status=1 and menuid=$menuid and roleid=$res")
                              ->update($data);
                          for($i=0;$i<$funnum;$i++){
                              unset($data);
                              $data['userid']=$userid;
                              $data['menuid']=$menuid;
                              $data['roleid']=$res;
                              $data['permissionsid']=$funvals[$i];
                              $data['permissions_name']=$funtexts[$i];
                              $data['attribute']=$funtags[$i];
                              $data['udate']=time();
                              $data['uuid']=$userid;
                              $data['status']=1;
                              $rp = db_func("admin_role_permissions", "dzm_");
                              $permissionsid=$rp->where("permissionsid=".$funvals[$i]." and menuid=$menuid and roleid=".$res."")->value('id');
                              if($permissionsid){
                                   Db::table('dzm_admin_role_permissions')
                                   ->where("id=$permissionsid")
                                   ->update($data);
                              }else{
                                  $data['cdate']=time();
                                  Db::table('dzm_admin_role_permissions')->insert($data);
                              }
                          }
                      }
                      ok_return('保存成功',1);
                    }
                }
            }
       }elseif($id){
            $r = db_func("admin_role", "dzm_");
            $res=$r->where("role_name='".$rolename."' and id!=".$_POST['id']." and status=1")->value('id');
            if($res){
                wrong_return('角色已存在',2);
            }else{
                $edit= getuserpermissions('Role', 'edit');
                if($edit){
                    $res=Db::table('dzm_admin_role')
                         ->where("id=$id")
                         ->update($data);
                    if($res){
                      //用户标签
                    if($funnum>0){
                        $funvals = explode('#', $funvals);
                          $funtexts = explode('#', $funtexts);
                          $funtags = explode('#', $funtags);
                        $rp = db_func("admin_role_permissions", "dzm_");
                        unset($data);
                        $data['status']='-1';
                        Db::table('dzm_admin_role_permissions')
                                   ->where("status=1 and menuid=$menuid and roleid=$id")
                                   ->update($data);
                        for($i=0;$i<$funnum;$i++){
                            unset($data);
                            $data['menuid']=$menuid;
                            $data['roleid']=$id;
                            $data['permissionsid']=$funvals[$i];
                            $data['permissions_name']=$funtexts[$i];
                            $data['attribute']=$funtags[$i];
                            $data['udate']=time();
                            $data['uuid']=$userid;
                            $data['status']=1;
                            $rp = db_func("admin_role_permissions", "dzm_");
                            $permissionsid=$rp->where("permissionsid='".$funvals[$i]."'  and menuid=$menuid and roleid=$id ")->value('id');
                            if($permissionsid){
                                Db::table('dzm_admin_role_permissions')
                                   ->where("id=$permissionsid")
                                   ->update($data);
                            }else{
                                $data['cdate']=time();
                                Db::table('dzm_admin_role_permissions')->insert($data);
                            }
                        }
                    }
                        ok_return('保存成功',3);
                    }
                }
            }
       }
    }


    /**
     * 获取菜单权限
     * 
     */
    public function getMenuFun(){
      $menuid = input('param.id');
      $html="";
      if($menuid){
        $userinfor=session('userinfor');
        $userid = $userinfor['id'];
        $orderby['udate']='asc';
        $where=" status=1 and  menuid=$menuid";
        $list = Db::table('dzm_admin_menu_permissions')->where($where)->order($orderby)->select();
        if($list){
          foreach ($list as $key => $value) {
            $key=$key+1;
            $html.="<li><label for='signOn".$key."'><input type='checkbox' id='signOn".$key."' signid='".$value['id']."'  tag='".$value['permissions_tag']."'  name='sign'><span>".$value['permissions_name']."</span></label></li>";
          }
        }
      }
      ok_return('','',$html);
    }


    public function getRoleMenuFun(){
      $menuid = input('param.menuid');
      $roleid = input('param.roleid');
      $html="";
      if($menuid&&$roleid){
        $userinfor=session('userinfor');
        $userid = $userinfor['id'];
        $orderby['udate']='asc';
        $where=" status=1 and  menuid=$menuid and roleid=$roleid ";
        $list = Db::table('dzm_admin_role_permissions')->where($where)->order($orderby)->select();
        if($list){
          foreach ($list as $key => $value) {
            $html.="<li funtag='".$value['attribute']."' funid='".$value['permissionsid']."' funname='".$value['permissions_name']."'><i>x</i>".$value['permissions_name']."</li>";
          }
        }
      }
      ok_return('','',$html);
    }


    public function delAjax(){
        $del= getuserpermissions('Role', 'del');
        $id = input('param.id');
        if(!empty($id)&&$del)
        {
            $data['status']=-1;
            $rst=Db::table('dzm_admin_role')
                ->where('id','eq',$id)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
        $this->ajaxReturn($rst);
    }

    public function delMoreAjax(){
        $del= getuserpermissions('Role', 'del');
        $ids = input('param.ids');
        if($ids&&$del)
        {
            $ids=substr($ids, 0,-1);
            $data['status']=-1;
            $rst=Db::table('dzm_admin_role')
                ->where('id','in',$ids)
                ->update($data);
            if($rst){
                ok_return('删除成功',1);
            }else{
                wrong_return('删除失败',2);
            }
        }
    }

     /**
     * 删除角色菜单权限
     *
     */
    public function delRoleMenuFun(){
       $del= getuserpermissions('Role', 'del');
       $menuid = input('param.menuid');
       $roleid = input('param.roleid');
       $permissionsid = input('param.permissionsid');
      if(!empty($menuid)&&!empty($roleid)&&!empty($permissionsid)&&$del)
      {
         $r = db_func("admin_role_permissions", "dzm_");
        $data['status']=-1;
        $data['udate']=time();
             $permissionsid=$r->where("permissionsid='".$permissionsid."'  and menuid=$menuid and roleid=$roleid ")->value('id');
              if($permissionsid){
                  $res=Db::table('dzm_admin_role_permissions')->where('id','eq',$permissionsid)->update($data);
                    if($res){
                      ok_return('删除成功',1);
                    }else{
                      wrong_return('删除失败',2);
                    }
              }
      }
    }
}
