<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function wrong_return($msg = 'failed', $code = -1, $ret_data = [])
{
    $arr = [
        'ret' => 1,
        'code' => strval($code),
        'msg' => $msg,
        'ret_data' => $ret_data
    ];
    die_json($arr, 1);
}

function ok_return($msg = 'success', $code = 1, $ret_data = [])
{
    $arr = [
        'ret' => 0,
        'code' => strval($code),
        'msg' => $msg,
        'ret_data' => $ret_data
    ];
    die_json($arr);
}

//实例化数据库,
function db_func($str = null, $pre_fix = null, $connect = null)
{
    if (!empty($connect)) \think\Db::connect($connect);
    if (!empty($pre_fix)) {
        return \think\Db::table($pre_fix . $str);
    }
    return \think\Db::name($str);
}

//输入json并且结束
function die_json($post, $type = null)
{
    die(rt_json($post, $type));
}

/*数组返回json
字符串返回code=字符串
type存在,返回中文**/
function rt_json($post, $type = null, $msg = null)
{

    if (is_array($post)) {
        if ($type) return json_encode($post, JSON_HEX_TAG|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        return json_encode($post,JSON_HEX_TAG|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    } else {
        $tmp = array(
            "code" => $post
        );
        if (!empty($msg)) {
            $tmp['msg'] = $tmp;
        }
        if ($type) return json_encode($tmp, JSON_UNESCAPED_UNICODE);
        return json_encode($tmp,JSON_HEX_TAG|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }
}

function relogin()
{
    if (session('?userinfor')) {
        $userinfor = session('userinfor');
        $logintime = $userinfor['logintime'];
        $longtime = $logintime-NOW_TIME;
    }else{
        $longtime = 0;
    }
    return $longtime;
}

function getuserpermissions($menu_tag, $key="")
{
    $access = false;
    $userinfor = session('userinfor');
    $userid = $userinfor['id'];
    $spuradmin = array(
        ''
    );
    if ($userinfor['is_admin'] == 3) {
        $access = true;
    } elseif ($userinfor['is_admin'] == 1 && ! in_array($menu_tag, $spuradmin)) {
        $access = true;
    } else {
        $m = db_func("admin_menu", "dzm_");
        $menuid = $m->where("menu_tag='" . $menu_tag . "' and status=1")->value('id');
        $u = db_func("admin_users", "dzm_");
        $list = $u->where("id='" . $userid . "'")->find();
        $rp = db_func("admin_role_permissions", "dzm_");
        if ($key) {
            $list2 = $rp->where("roleid='" . $list['roleid'] . "' and menuid='" . $menuid . "' and attribute='" . $key . "' and status=1 ")->find();
        } else {
            $list2 = $rp->where("roleid='" . $list['roleid'] . "' and menuid='" . $menuid . "' and status=1 ")->find();
        }
        if ($list2) {
            $access = true;
        }
    }
    return $access;
}

/**
 * 菜单下拉框树行结构
 *
 */
function getTreeLevel($cate, $html = '--', $pid = 0, $level = 0) {
    $arr = array();
    foreach ($cate as $k => $v) {
        if ($v['pid'] == $pid) {
            $v['level'] = $level + 1;
            if($pid>0)
            {
                $v['html'] = str_repeat('&nbsp;&nbsp;&nbsp;', $level).'|'.$html ;
            }else{
                $v['html'] = "";
            }
            $arr[] = $v;
            $arr = array_merge($arr, getTreeLevel($cate, $html, $v['id'], $level + 1));
        }
    }
    return $arr;
}

/**
 *
 * 获取某个分类的所有父分类
 */
function getParents($categorys,$catId){
    $tree=array();
    foreach($categorys as $item){
        if($item['id']==$catId){
            if($item['pid']>0)
                $tree=array_merge($tree,getParents($categorys,$item['pid']));
            $tree[]=$item;
            break;
        }
    }
    return $tree;
}

/**
 * 树行结构
 *
 */
function getTree($data, $pId)
{
    $tree = '';
    foreach($data as $k => $v)
    {
        if($v['pid'] == $pId)
        {
            $v['child'] = getTree($data, $v['id']);
            $tree[] = $v;
        }
    }
    return $tree;
}