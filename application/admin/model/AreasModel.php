<?php
namespace app\admin\model;
use think\Model;
use org\CacheKey;
Class AreasModel extends Model
{
	protected $table = 'dzm_areas';
    protected $pk = 'areaid';
    
    public function insert_data($data){
    	$r=$this->insert($data);
    	return $r;
    }

    public function update_row($data,$id)
    {
        if(!$id || !is_array($data))return false;
        $r=$this->update_data($data,"areaid='".$id."'");
        $this->clean_cache($id);
        return $r;
    }

    public function update_data($data, $where)
    {
        $r=$this->where($where)->update($data);
        return $r;
    }

    public function delete_data($id) {
        $data["status"] = -1;
        $r=$this->where('areaid','eq',$id)->update($data);
        $this->clean_cache($id);
        return $r;
    }

    public function get_data_all($where,$orderby)
    {
        return $this->where($where)->order($orderby)->select();
    }

    public function get_row_byid($id){
        if(!$id || !is_numeric($id))return;
        $data = '';
        $cachekey = new  \org\CacheKey;
        $key=$cachekey->get_admin_areas_key($id);
        $data=cache($key);
        if(!$data)
        {
            $data=$this->where("areaid='".$id."' and status=1 ")->find();
            if($data)
            {
                cache($key,$data,3600);
            }
        }
        return $data;
    }

    public function clean_cache($id)
    {
        $cachekey = new  \org\CacheKey;
        $key=$cachekey->get_admin_areas_key($id);
        cache($key,NULL);
    }

}