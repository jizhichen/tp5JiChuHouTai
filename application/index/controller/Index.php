<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\template\driver\File;
use org\Image;
use org\image\driver\Gd;
use org\Upload; //上传类
use org\CacheKey; //缓存类
class Index extends Common
{
    public function index()
    {
    	$cachekey = new  CacheKey();
    	 $cachekey->get_user_info_key(1);
    	return $this->fetch();
    }
}
