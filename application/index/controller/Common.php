<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace app\index\controller;
use think\Controller;
use think\Db;
class Common extends Controller {
        public $_cjv = 0;//config 中的 CSS_JS_VS 控制 js css 的版本号
        public $_site_url = '';//网站域名 http://wxxx.xxx.com
        public $_module='';
        public $_controller = '';//当前控制器
        public $_action = '';//当前操作
        public $_timestamp = '';
        public $check_access=true; //页面登陆权限
        public $pagesize = 100;
        public $imagedomain = '';
        protected function _initialize(){
            $this->imageupload=config('IMG_UPLOAD').'/upload/upload/index';
            $this->assign('imgupload',$this->imageupload);
        }
}
?>
