<?php
namespace app\index\model;
use think\Model;
Class UserModel extends Model
{
	protected $table = 'user';
    protected $pk = 'userid';
    
    public function insert_data($post){
    	$org = $cdate = '';
        extract($post);
    	//如果全部正确,执行写入数据
    	$data = array(
    			"org" => $org,
    			"cdate" => time()
    	);
    	$r=$this->insertGetId($data);
    	return $r;
    }

    public function update_data($data, $where)
    {
       
    }

    public function update_row($data,$id)
    {
        if(!$id || !is_array($data))return false;
        $rs=$this->update_data($data,"id='".$id."'");
        $this->clean_cache($id);
        return $rs;
    }


    public function delete_data($id,$uids) {
        $data["status"] = -1;
        $rs=$this->where("id='".$id."' and userid in".$uids."")->save($data);
        $this->clean_cache($id);
        return $rs;
    }

    public function get_data_all($where)
    {
        $orderby['cdate']='asc';
        return $this->where($where)->order($orderby)->select();
    }

    public function get_row_byid($id,$uids){
        if(!$id || !is_numeric($id))return;
        $cachekey = new  \org\CacheKey;
        $key=$cachekey->get_address_key($id);
        $data=S($key);
        if(!$data)
        {
            $data=$this->where("id='".$id."' and userid in".$uids." and status=1 ")->find();
            if($data)
            {
                S($key,$data,3600);
            }
        }
        return $data;
    }


    public function clean_cache($id)
    {
        $cachekey = new  \Think\CacheKey;
        $key=$cachekey->get_address_key($id);
        S($key,NULL);
    }


}