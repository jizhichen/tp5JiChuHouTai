<?php
namespace app\upload\controller;
use think\Controller;
use org\PicData;
class Upload extends Controller {
	public $check_access=false;
	public function index(){
		if($_FILES && $_FILES["Filedata"]["size"]>0)
		{
			$souce_file=$_FILES["Filedata"]["tmp_name"];
			$pic =  new \org\PicData;
			$data=$pic->upload_file($souce_file,$thum="",$width="",$height="",$watermark="");
			$json = json_encode($data);
			echo $json;
		}
    }
    public function delFile(){
        $img_path = $_POST['filePath'];
        $pic = new  \Think\PicData;
        $res = $pic->delFileResource($img_path);
        $return['status'] = $res;
        $this->ajaxReturn($return);
    }
}
